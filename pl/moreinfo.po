# Copyright (C) 2012 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2014, 2015, 2017.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2019-07-06 10:18+0200\n"
"PO-Revision-Date: 2017-06-16 20:39+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <debian-l10n-polish@lists.debian.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.5\n"

#. type: Attribute 'lang' of: <chapter>
#: en/moreinfo.dbk:8
msgid "en"
msgstr "pl"

#. type: Content of: <chapter><title>
#: en/moreinfo.dbk:9
msgid "More information on &debian;"
msgstr "Więcej informacji na temat projektu &debian;"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:11
msgid "Further reading"
msgstr "Dodatkowe informacje"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:13
msgid ""
"Beyond these release notes and the installation guide, further documentation "
"on Debian is available from the Debian Documentation Project (DDP), whose "
"goal is to create high-quality documentation for Debian users and "
"developers, such as the Debian Reference, Debian New Maintainers Guide, the "
"Debian FAQ, and many more.  For full details of the existing resources see "
"the <ulink url=\"&url-ddp;\">Debian Documentation website</ulink> and the "
"<ulink url=\"&url-wiki;\">Debian Wiki</ulink>."
msgstr ""
"Poza uwagami do wydana i przewodnikiem po instalacji dostępna jest również "
"dodatkowa dokumentacja na temat systemu Debian, pochodząca z Projektu "
"Dokumentacji Debiana (DDP), którego zadaniem jest tworzenie wysokiej jakości "
"dokumentacji dla użytkowników i deweloperów Debiana. Dostępne są dokumenty "
"Debian Reference, Debian New Maintainers' Guide i FAQ Debiana oraz wiele "
"innych. Pełny spis wszystkich zasobów można znaleźć na <ulink url=\"&url-ddp;"
"\">stronie z dokumentacją Debiana</ulink> (proszę zwrócić uwagę na odnośnik "
"do polskiej dokumentacji w dolnej części strony) i w <ulink url=\"&url-wiki;"
"\">Wiki Debiana</ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:23
msgid ""
"Documentation for individual packages is installed into <filename>/usr/share/"
"doc/<replaceable>package</replaceable></filename>.  This may include "
"copyright information, Debian specific details, and any upstream "
"documentation."
msgstr ""
"Dokumentacja poszczególnych pakietów jest instalowana do katalogów "
"<filename>/usr/share/doc/<replaceable>pakiet</replaceable></filename>. Mogą "
"być to informacje o prawach autorskich, detale dystrybucji Debian lub "
"dokumentacja z projektu macierzystego."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:31
msgid "Getting help"
msgstr "Pomoc"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:33
msgid ""
"There are many sources of help, advice, and support for Debian users, though "
"these should only be considered after researching the issue in available "
"documentation.  This section provides a short introduction to these sources "
"which may be helpful for new Debian users."
msgstr ""
"Istnieje wiele źródeł pomocy, rady i wsparcia dla użytkowników Debiana, lecz "
"powinno się z nich korzystać dopiero wtedy, gdy przeszukało się dostępną "
"dokumentację, która mogła zawierać wyjaśnienie problemu. Niniejszy rozdział "
"jest krótkim wprowadzeniem który może okazać się pomocny dla nowych "
"użytkowników Debiana."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:39
msgid "Mailing lists"
msgstr "Listy dyskusyjne"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:41
msgid ""
"The mailing lists of most interest to Debian users are the debian-user list "
"(English) and other debian-user-<replaceable>language</replaceable> lists "
"(for other languages).  For information on these lists and details of how to "
"subscribe see <ulink url=\"&url-debian-list-archives;\"></ulink>.  Please "
"check the archives for answers to your question prior to posting and also "
"adhere to standard list etiquette."
msgstr ""
"Najbardziej interesującymi dla użytkowników Debiana listami dyskusyjnymi są: "
"debian-user (angielska) i listy dla poszczególnych języków: debian-user-"
"<replaceable>język</replaceable> (np. debian-user-polish - polska lista). "
"Więcej informacji i szczegóły na temat subskrypcji zawiera strona <ulink url="
"\"&url-debian-list-archives;\"></ulink>. Przed zamieszczeniem nowej "
"wiadomości prosimy o uprzednie przeszukanie archiwów listy, które może już "
"zawierać odpowiedź na zadawane pytanie; proszę również pamiętać o zachowaniu "
"netykiety."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:51
msgid "Internet Relay Chat"
msgstr "IRC"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:53
msgid ""
"Debian has an IRC channel dedicated to support and aid for Debian users, "
"located on the OFTC IRC network.  To access the channel, point your favorite "
"IRC client at irc.debian.org and join <literal>#debian</literal>."
msgstr ""
"Debian ma kanał IRC, który jest przeznaczony do pomocy użytkownikom Debiana "
"(w sieci IRC OFTC). Dostęp do kanału można uzyskać ze swojego klienta IRC, "
"pod adresem irc.debian.org, wybierając kanał <literal>#debian</literal>. "
"Obowiązującym językiem jest angielski."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:58
msgid ""
"Please follow the channel guidelines, respecting other users fully.  The "
"guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
"Wiki</ulink>."
msgstr ""
"Proszę przestrzegać zasad kanału respektując prawa innych użytkowników. "
"Wytyczne są dostępne na <ulink url=\"&url-wiki;DebianIRC\">Wiki Debiana</"
"ulink>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:63
msgid ""
"For more information on OFTC please visit the <ulink url=\"&url-irc-host;"
"\">website</ulink>."
msgstr ""
"Więcej informacji o OFTC można znaleźć na odpowiedniej <ulink url=\"&url-irc-"
"host;\">stronie internetowej</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:71
msgid "Reporting bugs"
msgstr "Zgłaszanie błędów"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:73
msgid ""
"We strive to make Debian a high-quality operating system; however that does "
"not mean that the packages we provide are totally free of bugs.  Consistent "
"with Debian's <quote>open development</quote> philosophy and as a service to "
"our users, we provide all the information on reported bugs at our own Bug "
"Tracking System (BTS).  The BTS can be browsed at <ulink url=\"&url-bts;\"></"
"ulink>."
msgstr ""
"Staramy się, aby Debian był systemem operacyjnym wysokiej jakości, jednak "
"nie znaczy to, że udostępniane pakiety są całkowicie wolne od błędów. "
"Zgodnie z filozofią <quote>otwartego rozwoju</quote> Debiana oraz jako "
"usługa dla naszych użytkowników, dostarczamy wszystkie informacje o "
"zgłoszonych błędach w naszym Systemie Śledzenia Błędów (Bug Tracking System "
"- BTS). BTS można przeglądać pod adresem <ulink url=\"&url-bts;\"></ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:81
msgid ""
"If you find a bug in the distribution or in packaged software that is part "
"of it, please report it so that it can be properly fixed for future "
"releases.  Reporting bugs requires a valid e-mail address.  We ask for this "
"so that we can trace bugs and developers can get in contact with submitters "
"should additional information be needed."
msgstr ""
"W przypadku znalezienia błędu w dystrybucji lub spakietowanym programie "
"będącym jej częścią prosimy o zgłoszenie błędu, dzięki czemu będzie mógł "
"zostać poprawiony w kolejnych wydaniach. Zgłaszanie błędów wymaga poprawnego "
"adresu poczty elektronicznej. Dzięki niemu możliwe jest śledzenie błędów i "
"kontakt deweloperów ze zgłaszającym, w razie potrzeby uzyskania dodatkowych "
"informacji."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:88
msgid ""
"You can submit a bug report using the program <command>reportbug</command> "
"or manually using e-mail.  You can find out more about the Bug Tracking "
"System and how to use it by reading the reference documentation (available "
"at <filename>/usr/share/doc/debian</filename> if you have <systemitem role="
"\"package\">doc-debian</systemitem> installed) or online at the <ulink url="
"\"&url-bts;\">Bug Tracking System</ulink>."
msgstr ""
"Błędy można zgłaszać za pomocą programu <command>reportbug</command> lub "
"ręcznie, za pomocą poczty elektronicznej. Więcej informacji o Systemie "
"Śledzenia Błędów i sposobie korzystania z niego, zawiera dokumentacja "
"(dostępna w <filename>/usr/share/doc/debian</filename>, jeśli zainstalowano "
"pakiet <systemitem role=\"package\">doc-debian</systemitem>) lub strona "
"<ulink url=\"&url-bts;\">System Śledzenia Błędów</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:98
msgid "Contributing to Debian"
msgstr "Uczestnictwo w rozwoju Debiana"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:100
#, fuzzy
#| msgid ""
#| "You do not need to be an expert to contribute to Debian.  By assisting "
#| "users with problems on the various user support <ulink url=\"&url-debian-"
#| "list-archives;\">lists</ulink> you are contributing to the community.  "
#| "Identifying (and also solving) problems related to the development of the "
#| "distribution by participating on the development <ulink url=\"&url-debian-"
#| "list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
#| "Debian's high-quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
#| "ulink> and help developers track them down and fix them.  The tool "
#| "<systemitem role=\"package\">how-can-i-help</systemitem> helps you to "
#| "find suitable reported bugs to work on.  If you have a way with words "
#| "then you may want to contribute more actively by helping to write <ulink "
#| "url=\"&url-ddp-svn-info;\">documentation</ulink> or <ulink url=\"&url-"
#| "debian-i18n;\">translate</ulink> existing documentation into your own "
#| "language."
msgid ""
"You do not need to be an expert to contribute to Debian.  By assisting users "
"with problems on the various user support <ulink url=\"&url-debian-list-"
"archives;\">lists</ulink> you are contributing to the community.  "
"Identifying (and also solving) problems related to the development of the "
"distribution by participating on the development <ulink url=\"&url-debian-"
"list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
"Debian's high-quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
"ulink> and help developers track them down and fix them.  The tool "
"<systemitem role=\"package\">how-can-i-help</systemitem> helps you to find "
"suitable reported bugs to work on.  If you have a way with words then you "
"may want to contribute more actively by helping to write <ulink url=\"&url-"
"ddp-vcs-info;\">documentation</ulink> or <ulink url=\"&url-debian-i18n;"
"\">translate</ulink> existing documentation into your own language."
msgstr ""
"Nie trzeba być ekspertem, aby wspomóc rozwój Debiana. Pomagając rozprawić "
"się użytkownikom z ich problemami, za pomocą wielu dostępnych <ulink url="
"\"&url-debian-list-archives;\">list dyskusyjnych</ulink> uczestniczy się w "
"rozwoju społeczności. Rozpoznawanie (jak również rozwiązywanie) problemów "
"związanych z rozwojem dystrybucji, przez udział w <ulink url=\"&url-debian-"
"list-archives;\">listach deweloperskich</ulink> jest również niezwykle "
"pomocne. Aby utrzymać wysoką jakość dystrybucji Debian prosimy o <ulink url="
"\"&url-bts;\">zgłaszanie błędów</ulink> i pomoc deweloperom w ich śledzeniu "
"i naprawianiu. Narzędzie <systemitem role=\"package\">how-can-i-help</"
"systemitem> może pomóc w znalezieniu błędów które można naprawić. Osoby "
"czujące się dobre w pisaniu, mogą pomóc w tworzeniu <ulink url=\"&url-ddp-"
"svn-info;\">dokumentacji</ulink> lub <ulink url=\"&url-debian-i18n;"
"\">tłumaczeniu</ulink> istniejącej na swój język."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:117
msgid ""
"If you can dedicate more time, you could manage a piece of the Free Software "
"collection within Debian.  Especially helpful is if people adopt or maintain "
"items that people have requested for inclusion within Debian.  The <ulink "
"url=\"&url-wnpp;\">Work Needing and Prospective Packages database</ulink> "
"details this information.  If you have an interest in specific groups then "
"you may find enjoyment in contributing to some of Debian's <ulink url=\"&url-"
"debian-projects;\">subprojects</ulink> which include ports to particular "
"architectures and <ulink url=\"&url-debian-blends;\">Debian Pure Blends</"
"ulink> for specific user groups, among many others."
msgstr ""
"Jeśli ma się nieco więcej czasu można poświęcić go jakiejś części Wolnego "
"Oprogramowania w Debianie. Szczególnie pożądane jest, aby adoptować lub "
"zacząć opiekować się pakietami, o których dodanie prosili inni użytkownicy. "
"Baza <ulink url=\"&url-wnpp;\">Work Needing and Prospective Packages</ulink> "
"dostarcza szczegółowych informacji w tym zakresie. Jeśli jest się "
"szczególnie zainteresowanym jakąś konkretną dziedziną to można włożyć swój "
"wkład w któryś z <ulink url=\"&url-debian-projects;\">podprojektów</ulink> "
"Debiana, które zajmują się również portami na różne architektury lub w tzw. "
"<ulink url=\"&url-debian-blends;\">Debian Pure Blends</ulink>, "
"przeznaczonych dla określonych grup użytkowników, albo w wiele innych."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:128
msgid ""
"In any case, if you are working in the free software community in any way, "
"as a user, programmer, writer, or translator you are already helping the "
"free software effort.  Contributing is rewarding and fun, and as well as "
"allowing you to meet new people it gives you that warm fuzzy feeling inside."
msgstr ""
"W każdym razie, niezależnie od tego, czy działa się na rzecz społeczności "
"Wolnego Oprogramowania jako użytkownik, programista, osoba zajmująca się "
"pisaniem dokumentacji lub tłumaczeniami, już teraz dokłada się swoją "
"cegiełkę w budowie Wolnego Oprogramowania. Taka praca daje dużo satysfakcji "
"i radości oraz pozwala na poznanie nowych ludzi."
